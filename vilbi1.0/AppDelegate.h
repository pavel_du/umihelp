//
//  AppDelegate.h
//  vilbi1.0
//
//  Created by Pavel Dunyashev on 13/07/16.
//  Copyright © 2016 Pavel Dunyashev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

